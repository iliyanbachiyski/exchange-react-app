type TickerDetails = {
  symbol: string;
  priceChange: number;
  priceChangePercent: number;
  lastPrice: number;
  bidPrice: number;
  askPrice: number;
  highPrice: number;
  lowPrice: number;
  volume: number;
  openTime: Date;
  closeTime: Date;
};

export type TickerDetailsRaw = {
  symbol: string;
  priceChange: string;
  priceChangePercent: string;
  lastPrice: string;
  bidPrice: string;
  askPrice: string;
  highPrice: string;
  lowPrice: string;
  volume: string;
  openTime: number;
  closeTime: number;
};

export default TickerDetails;
