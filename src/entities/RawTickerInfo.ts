export type BinanceTickerInfoRaw = {
  symbol: string;
  price: string;
};

export type BinanceTradeDetailsRaw = {
  price: string;
  qty: string;
  time: number;
  isBuyerMaker: boolean;
};

export type BitfinexTickerInfoRaw = {
  last_price: string;
};

export type BitfinexTradeDetailsRaw = {
  price: string;
  amount: string;
  timestamp: number;
  type: "buy" | "sell";
};

export type HuobiTickerInfoRaw = {
  tick: {
    close: number;
  };
  status: "ok" | "error";
};

export type HuobiTradeDetailsRaw = {
  data: {
    ts: number;
    price: number;
    amount: number;
    direction: "buy" | "sell";
  }[];
};

export type KrakenTickerInfoRaw = {
  result: {
    [key: string]: {
      p: string[];
    };
  };
  error: string[];
};
