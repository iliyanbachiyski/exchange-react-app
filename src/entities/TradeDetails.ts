type TradeDetails = {
  price: number;
  quantity: number;
  time: number;
  isBuy: boolean;
};

export default TradeDetails;
