export enum ExchangeProvider {
  Binance = "binance",
  Bitfinex = "bitfinex",
  Huobi = "huobi",
  Kraken = "kraken",
}

type TickerInfo = {
  symbol: string;
  provider: ExchangeProvider;
  isSupported: boolean;
  price: number;
};

export type TickerInfoSummary = {
  [ExchangeProvider.Binance]?: TickerInfo;
  [ExchangeProvider.Bitfinex]?: TickerInfo;
  [ExchangeProvider.Huobi]?: TickerInfo;
  [ExchangeProvider.Kraken]?: TickerInfo;
};

export default TickerInfo;
