import axios, { AxiosPromise, AxiosResponse } from "axios";
import {
  BINANCE_TICKER_PRICE_URL,
  BITFINEX_TICKER_PRICE_URL,
  HUOBI_TICKER_PRICE_URL,
  KRAKEN_TICKER_PRICE_URL,
  BINANCE_TRADE_DETAILS_URL,
  BITFINEX_TRADE_DETAILS_URL,
  HUOBI_TRADE_DETAILS_URL,
  KRAKEN_TRADE_DETAILS_URL,
  TICKER_DETAILS_URL,
} from "../config";
import {
  BinanceTickerInfoRaw,
  BinanceTradeDetailsRaw,
  BitfinexTickerInfoRaw,
  BitfinexTradeDetailsRaw,
  HuobiTickerInfoRaw,
  HuobiTradeDetailsRaw,
  KrakenTickerInfoRaw,
} from "../entities/RawTickerInfo";
import TickerDetails, { TickerDetailsRaw } from "../entities/TickerDetails";
import { ExchangeProvider, TickerInfoSummary } from "../entities/TickerInfo";
import TradeDetails from "../entities/TradeDetails";

const TRADE_LIMITS = 20;

const parseBinanceInfo = (
  result:
    | PromiseRejectedResult
    | PromiseFulfilledResult<AxiosResponse<BinanceTickerInfoRaw>>
    | any,
  symbol: string,
  data: TickerInfoSummary
) => {
  if (result.status === "fulfilled") {
    data.binance = {
      symbol: symbol,
      price: Number((result.value.data as BinanceTickerInfoRaw).price),
      provider: ExchangeProvider.Binance,
      isSupported: true,
    };
  } else {
    data.binance = {
      symbol: symbol,
      provider: ExchangeProvider.Binance,
      isSupported: false,
      price: 0,
    };
  }
};

const parseBitfinexInfo = (
  result:
    | PromiseRejectedResult
    | PromiseFulfilledResult<AxiosResponse<BitfinexTickerInfoRaw>>
    | any,
  symbol: string,
  data: TickerInfoSummary
) => {
  if (result.status === "fulfilled") {
    data.bitfinex = {
      symbol: symbol,
      price: Number(result.value.data.last_price),
      provider: ExchangeProvider.Bitfinex,
      isSupported: true,
    };
  } else {
    data.bitfinex = {
      symbol: symbol,
      provider: ExchangeProvider.Bitfinex,
      isSupported: false,
      price: 0,
    };
  }
};

const parseKrakenInfo = (
  result:
    | PromiseRejectedResult
    | PromiseFulfilledResult<AxiosResponse<KrakenTickerInfoRaw>>
    | any,
  symbol: string,
  data: TickerInfoSummary
) => {
  if (
    result.status === "fulfilled" &&
    !(result.value.data as KrakenTickerInfoRaw).error.length
  ) {
    const key = Object.keys(result.value.data.result)[0];
    data.kraken = {
      symbol: symbol,
      price: Number(result.value.data.result[key].p[0]),
      provider: ExchangeProvider.Kraken,
      isSupported: true,
    };
  } else {
    data.kraken = {
      symbol: symbol,
      provider: ExchangeProvider.Kraken,
      isSupported: false,
      price: 0,
    };
  }
};

const parseHuobiInfo = (
  result:
    | PromiseRejectedResult
    | PromiseFulfilledResult<AxiosResponse<HuobiTickerInfoRaw>>
    | any,
  symbol: string,
  data: TickerInfoSummary
) => {
  if (
    result.status === "fulfilled" &&
    (result.value.data as HuobiTickerInfoRaw).status === "ok"
  ) {
    data.huobi = {
      symbol: symbol,
      price: result.value.data.tick.close,
      provider: ExchangeProvider.Huobi,
      isSupported: true,
    };
  } else {
    data.huobi = {
      symbol: symbol,
      provider: ExchangeProvider.Huobi,
      isSupported: false,
      price: 0,
    };
  }
};

const fetchInfoFromBinance = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("symbol", ticker.toUpperCase());
  const url = `${BINANCE_TICKER_PRICE_URL}?${urlSearchParams.toString()}`;
  return axios.get(url) as AxiosPromise<BinanceTickerInfoRaw>;
};

const fetchInfoFromBitfinex = (ticker: string) => {
  return axios.get(
    `${BITFINEX_TICKER_PRICE_URL}/${ticker.toLowerCase()}`
  ) as AxiosPromise<BitfinexTickerInfoRaw>;
};

const fetchInfoFromHuobi = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("symbol", ticker.toLowerCase());
  const url = `${HUOBI_TICKER_PRICE_URL}?${urlSearchParams.toString()}`;
  return axios.get(url) as AxiosPromise<HuobiTickerInfoRaw>;
};

const fetchInfoFromKraken = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("pair", ticker.toUpperCase());
  const url = `${KRAKEN_TICKER_PRICE_URL}?${urlSearchParams.toString()}`;
  return axios.get(url) as AxiosPromise<KrakenTickerInfoRaw>;
};

export const fetchTickerInfo = (ticker: string): Promise<TickerInfoSummary> => {
  let formatedTicker = ticker;
  if (formatedTicker.indexOf("/") !== -1) {
    formatedTicker = formatedTicker.replace("/", "");
  }
  const binancePromise = fetchInfoFromBinance(formatedTicker);
  const bitfinexPromise = fetchInfoFromBitfinex(formatedTicker);
  const huobiPromise = fetchInfoFromHuobi(formatedTicker);
  const krakenPromise = fetchInfoFromKraken(formatedTicker);
  return new Promise((resolve, reject) => {
    Promise.allSettled([
      binancePromise,
      bitfinexPromise,
      huobiPromise,
      krakenPromise,
    ])
      .then((results) => {
        const response: TickerInfoSummary = {};
        results.forEach((result, index) => {
          switch (index) {
            case 0:
              parseBinanceInfo(result, ticker, response);
              break;
            case 1:
              parseBitfinexInfo(result, ticker, response);
              break;
            case 2:
              parseHuobiInfo(result, ticker, response);
              break;
            case 3:
              parseKrakenInfo(result, ticker, response);
              break;
          }
        });
        resolve(response);
      })
      .catch(reject);
  });
};

const fetchBinanceTradeDetails = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("symbol", ticker);
  urlSearchParams.append("limit", String(TRADE_LIMITS));
  const url = `${BINANCE_TRADE_DETAILS_URL}?${urlSearchParams.toString()}`;
  return new Promise<TradeDetails[]>((resolve, reject) => {
    axios
      .get(url)
      .then(({ data }) => {
        resolve(parseBinanceTradeDetails(data));
      })
      .catch(reject);
  });
};

const parseBinanceTradeDetails = (
  data: BinanceTradeDetailsRaw[]
): TradeDetails[] => {
  return data.map((el) => ({
    price: Number(el.price) || 0,
    quantity: Number(el.qty),
    time: el.time,
    isBuy: el.isBuyerMaker,
  }));
};

const fetchBitfinexTradeDetails = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("limit_trades", String(TRADE_LIMITS));
  const url = `${BITFINEX_TRADE_DETAILS_URL}/${ticker.toLowerCase()}?${urlSearchParams.toString()}`;
  return new Promise<TradeDetails[]>((resolve, reject) => {
    axios
      .get(url)
      .then(({ data }) => {
        resolve(parseBitfinexTradeDetails(data));
      })
      .catch(reject);
  });
};

const parseBitfinexTradeDetails = (
  data: BitfinexTradeDetailsRaw[]
): TradeDetails[] => {
  return data.map((el) => ({
    price: Number(el.price) || 0,
    quantity: Number(el.amount),
    time: el.timestamp,
    isBuy: el.type === "buy",
  }));
};

const fetchHuobiTradeDetails = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("symbol", ticker.toLowerCase());
  urlSearchParams.append("size", String(TRADE_LIMITS));
  const url = `${HUOBI_TRADE_DETAILS_URL}?${urlSearchParams.toString()}`;
  return new Promise<TradeDetails[]>((resolve, reject) => {
    axios
      .get(url)
      .then(({ data: response }) => {
        resolve(parseHuobiTradeDetails(response.data));
      })
      .catch(reject);
  });
};

const parseHuobiTradeDetails = (
  data: HuobiTradeDetailsRaw[]
): TradeDetails[] => {
  return data.map((el) => ({
    price: el.data[0].price || 0,
    quantity: el.data[0].amount,
    time: el.data[0].ts,
    isBuy: el.data[0].direction === "buy",
  }));
};

const fetchKrakenTradeDetails = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("pair", ticker);
  const url = `${KRAKEN_TRADE_DETAILS_URL}?${urlSearchParams.toString()}`;
  return new Promise<TradeDetails[]>((resolve, reject) => {
    axios
      .get(url)
      .then(({ data }) => {
        const key = Object.keys(data.result)[0];
        const limitData = data.resul[key].slice(0, TRADE_LIMITS);
        parseKrakenTradeDetails(limitData);
      })
      .catch(reject);
  });
};

const parseKrakenTradeDetails = (data: any[]): TradeDetails[] => {
  return data.map((el) => ({
    price: Number(el[0]) || 0,
    quantity: Number(el[1]) || 0,
    time: el[2],
    isBuy: el[3] === "b",
  }));
};

export const fetchTradeDetails = (
  ticker: string,
  exchange: ExchangeProvider
) => {
  let formatedTicker = ticker;
  if (formatedTicker.indexOf("/") !== -1) {
    formatedTicker = formatedTicker.replace("/", "");
  }
  switch (exchange) {
    case ExchangeProvider.Binance:
      return fetchBinanceTradeDetails(formatedTicker);
    case ExchangeProvider.Bitfinex:
      return fetchBitfinexTradeDetails(formatedTicker);
    case ExchangeProvider.Huobi:
      return fetchHuobiTradeDetails(formatedTicker);
    case ExchangeProvider.Kraken:
      return fetchKrakenTradeDetails(formatedTicker);
  }
};

const parseTickerDetails = (data: TickerDetailsRaw): TickerDetails => {
  return {
    symbol: data.symbol,
    priceChange: Number(data.priceChange) || 0,
    priceChangePercent: Number(data.priceChangePercent) || 0,
    lastPrice: Number(data.lastPrice) || 0,
    bidPrice: Number(data.bidPrice) || 0,
    askPrice: Number(data.askPrice) || 0,
    highPrice: Number(data.highPrice) || 0,
    lowPrice: Number(data.lowPrice) || 0,
    volume: Number(data.volume) || 0,
    openTime: new Date(data.openTime),
    closeTime: new Date(data.closeTime),
  };
};

export const fetchTickerDetails = (ticker: string) => {
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.append("symbol", ticker.toUpperCase());
  return new Promise<TickerDetails>((resolve, reject) => {
    axios
      .get(`${TICKER_DETAILS_URL}?${urlSearchParams.toString()}`)
      .then(({ data }) => {
        resolve(parseTickerDetails(data));
      })
      .catch(reject);
  });
};
