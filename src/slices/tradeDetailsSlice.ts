import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ExchangeProvider } from "../entities/TickerInfo";
import TradeDetails from "../entities/TradeDetails";
import type { RootState } from "../store";

// Define a type for the slice state
interface TickerDetailsState {
  details: TradeDetails[];
  exchange: ExchangeProvider | null;
  isFetchInProgress: boolean;
  error?: string;
}

// Define the initial state using that type
const initialState: TickerDetailsState = {
  details: [],
  exchange: null,
  isFetchInProgress: true,
};

export const tradeDetailsSlice = createSlice({
  name: "tradeDetails",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    setTradeDetails: (state, action: PayloadAction<TradeDetails[]>) => {
      state.details = action.payload;
      state.isFetchInProgress = false;
    },
    setFetchInProgress: (state) => {
      state.details = [];
      state.isFetchInProgress = true;
    },
    setError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.isFetchInProgress = false;
    },
    setTradeDetailsExchange: (
      state,
      action: PayloadAction<ExchangeProvider>
    ) => {
      state.exchange = action.payload;
    },
    clearError: (state) => {
      state.error = undefined;
    },
    clearDetails: (state) => {
      state.details = [];
      state.isFetchInProgress = true;
    },
  },
});

export const {
  setTradeDetails,
  setFetchInProgress,
  setError,
  clearError,
  setTradeDetailsExchange,
  clearDetails,
} = tradeDetailsSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectDetails = (state: RootState) => state.tradeDetails.details;
export const selectError = (state: RootState) => state.tradeDetails.error;
export const selectExchange = (state: RootState) => state.tradeDetails.exchange;
export const selectIsFetchInProgress = (state: RootState) =>
  state.tradeDetails.isFetchInProgress;
export const selectTicker = (state: RootState) => {
  if (state.tradeDetails.exchange && state.tickerDetails.summary) {
    return state.tickerDetails.summary[state.tradeDetails.exchange]?.symbol;
  }
  return null;
};

export default tradeDetailsSlice.reducer;
