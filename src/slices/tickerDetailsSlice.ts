import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { fetchTickerInfo } from "../api";
import { TickerInfoSummary } from "../entities/TickerInfo";
import type { AppThunk, RootState } from "../store";

// Define a type for the slice state
interface TickerDetailsState {
  summary: TickerInfoSummary | null;
  isFetchInProgress: boolean;
  error?: string;
}

// Define the initial state using that type
const initialState: TickerDetailsState = {
  summary: null,
  isFetchInProgress: false,
};

export const tickerDetailsSlice = createSlice({
  name: "tickerDetails",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    setSummary: (state, action: PayloadAction<TickerInfoSummary>) => {
      state.summary = action.payload;
      state.isFetchInProgress = false;
    },
    clearSummary: (state) => {
      state.summary = null;
      state.isFetchInProgress = false;
    },
    setFetchInProgress: (state) => {
      state.isFetchInProgress = true;
    },
    setError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.isFetchInProgress = false;
    },
    clearError: (state) => {
      state.error = undefined;
    },
  },
});

export const ensureTickerInfo =
  (ticker: string): AppThunk<void> =>
  (dispatch) => {
    dispatch(setFetchInProgress());
    dispatch(clearError());
    fetchTickerInfo(ticker)
      .then((data) => {
        const hasPairMatch = Object.values(data).some(
          (item) => item.isSupported
        );
        if (!hasPairMatch) {
          dispatch(setError("The pair was not found! Please try again!"));
          dispatch(clearSummary());
        } else {
          dispatch(clearError());
          dispatch(setSummary(data));
        }
      })
      .catch(() => {
        dispatch(setError("Something went wrong! Please try again!"));
        dispatch(clearSummary());
      });
  };

export const {
  setSummary,
  clearSummary,
  setFetchInProgress,
  setError,
  clearError,
} = tickerDetailsSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectSummary = (state: RootState) => state.tickerDetails.summary;
export const selectError = (state: RootState) => state.tickerDetails.error;
export const selectIsFetchInProgress = (state: RootState) =>
  state.tickerDetails.isFetchInProgress;

export default tickerDetailsSlice.reducer;
