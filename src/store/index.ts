import {
  configureStore,
  ThunkAction,
  Action,
  combineReducers,
} from "@reduxjs/toolkit";
import tickerDetailsReducer from "../slices/tickerDetailsSlice";
import tradeDetailsReducer from "../slices/tradeDetailsSlice";

const combinedReducers = combineReducers({
  tickerDetails: tickerDetailsReducer,
  tradeDetails: tradeDetailsReducer,
});

export const store = configureStore({
  reducer: combinedReducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {tickerDetails: tickerDetailsReducer, tradeDetails: tradeDetails}
export type AppDispatch = typeof store.dispatch;
export type AppState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;

export default store;
