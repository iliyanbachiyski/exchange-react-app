import ClearRounded from "@mui/icons-material/ClearRounded";
import SearchOutlined from "@mui/icons-material/SearchOutlined";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import { useCallback, useEffect, useState } from "react";
import {
  clearSummary,
  ensureTickerInfo,
  selectError,
  selectIsFetchInProgress,
  selectSummary,
  clearError,
} from "../slices/tickerDetailsSlice";
import { useAppDispatch, useAppSelector } from "../store/hooks";

let fetchTickerInfoInterval: NodeJS.Timer;

type OwnProps = {
  onSearch: (ticker: string) => void;
};

const SearchBar = ({ onSearch }: OwnProps) => {
  const dispatch = useAppDispatch();
  const summary = useAppSelector(selectSummary);
  const isLoading = useAppSelector(selectIsFetchInProgress);
  const error = useAppSelector(selectError);
  const [ticker, setTicker] = useState("");

  useEffect(() => {
    if (summary && ticker) {
      if (fetchTickerInfoInterval) {
        clearInterval(fetchTickerInfoInterval);
      }
      fetchTickerInfoInterval = setInterval(() => {
        dispatch(ensureTickerInfo(ticker));
      }, 5000);
      return () => {
        clearInterval(fetchTickerInfoInterval);
      };
    }
  }, [summary, ticker]);

  const handleTickerChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setTicker(event.target.value);
    },
    []
  );

  const handleClearClick = useCallback(() => {
    setTicker("");
    dispatch(clearError());
    dispatch(clearSummary());
  }, []);

  const handleSearchClick = () => {
    clearInterval(fetchTickerInfoInterval);
    onSearch(ticker);
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      flexDirection="column"
      style={{ width: "50%" }}
      margin="auto"
    >
      <TextField
        type="text"
        margin="normal"
        placeholder="Example: BTC/USDT"
        fullWidth
        error={!!error}
        value={ticker}
        onChange={handleTickerChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                size="small"
                onClick={handleClearClick}
                disabled={!ticker}
                edge="end"
              >
                <ClearRounded />
              </IconButton>
            </InputAdornment>
          ),
        }}
        label="Search Market"
      />
      <Button
        variant="contained"
        startIcon={<SearchOutlined />}
        onClick={handleSearchClick}
        disabled={!ticker || isLoading}
        fullWidth
      >
        Search
      </Button>
    </Box>
  );
};

export default SearchBar;
