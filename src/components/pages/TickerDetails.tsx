import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { fetchTickerDetails } from "../../api";
import TickerDetails from "../../entities/TickerDetails";

const TickerDetailsPage = () => {
  let { ticker } = useParams();
  const [details, setDetails] = useState<TickerDetails>();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    handleFetchTickerDetails();
  }, [ticker]);

  const handleFetchTickerDetails = () => {
    if (error) {
      setError("");
    }
    if (!loading) {
      setLoading(true);
    }
    if (ticker) {
      fetchTickerDetails(ticker)
        .then((data) => setDetails(data))
        .catch(() => {
          setDetails(undefined);
          setError("Something went wrong! Please try again.");
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Typography variant="h5">24hr Ticker Price Change Details</Typography>
      {details && (
        <TableContainer style={{ margin: "16px 0" }} component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell align="center">
                  <b>Symbol</b>
                </TableCell>
                <TableCell align="center">{details.symbol}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Price change</b>
                </TableCell>
                <TableCell
                  align="center"
                  style={{ color: details.priceChange < 0 ? "red" : "green" }}
                >
                  {details.priceChange}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Price Change (%)</b>
                </TableCell>
                <TableCell
                  align="center"
                  style={{
                    color: details.priceChangePercent < 0 ? "red" : "green",
                  }}
                >
                  {details.priceChangePercent.toFixed()}%
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Last Price</b>
                </TableCell>
                <TableCell align="center">{details.lastPrice}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Bid Price</b>
                </TableCell>
                <TableCell align="center">{details.bidPrice}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Ask Price</b>
                </TableCell>
                <TableCell align="center">{details.askPrice}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>High Price</b>
                </TableCell>
                <TableCell align="center">{details.highPrice}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Low Price</b>
                </TableCell>
                <TableCell align="center">{details.lowPrice}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Volume</b>
                </TableCell>
                <TableCell align="center">{details.volume}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Open Time</b>
                </TableCell>
                <TableCell align="center">
                  {details.openTime.toLocaleString()}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="center">
                  <b>Close Time</b>
                </TableCell>
                <TableCell align="center">
                  {details.closeTime.toLocaleString()}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      )}
      {loading && <CircularProgress sx={{ marginY: 2 }} size={50} />}
      {error && (
        <Box
          display="flex"
          flexDirection="column"
          alignItems="center"
          sx={{ marginY: 2 }}
        >
          <Typography variant="subtitle1" color="red" align="center">
            {error}
          </Typography>
          <Button
            fullWidth
            style={{ maxWidth: "180px" }}
            sx={{ marginY: 2 }}
            variant="contained"
            onClick={handleFetchTickerDetails}
          >
            Try again
          </Button>
        </Box>
      )}
    </Box>
  );
};

export default TickerDetailsPage;
