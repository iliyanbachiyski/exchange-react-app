import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  clearError,
  clearSummary,
  ensureTickerInfo,
  selectSummary,
} from "../../slices/tickerDetailsSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import TickerDetailsSummary from "../TickerDetailsSummary";

let fetchTickerInfoInterval: NodeJS.Timer;

const TickerDetailsSummaryPage = () => {
  const { ticker } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const summary = useAppSelector(selectSummary);

  useEffect(() => {
    if (summary && ticker) {
      if (fetchTickerInfoInterval) {
        clearInterval(fetchTickerInfoInterval);
      }
      fetchTickerInfoInterval = setInterval(() => {
        dispatch(ensureTickerInfo(ticker));
      }, 5000);
    }
    return () => {
      clearInterval(fetchTickerInfoInterval);
    };
  }, [summary, ticker]);

  useEffect(() => {
    if (ticker) {
      dispatch(ensureTickerInfo(ticker));
    }
  }, [ticker]);

  const handleHomeClick = () => {
    if (fetchTickerInfoInterval) {
      clearInterval(fetchTickerInfoInterval);
    }
    dispatch(clearSummary());
    dispatch(clearError());
    navigate("/");
  };

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <TickerDetailsSummary />
      <Button
        sx={{ marginY: 2, width: 150 }}
        variant="contained"
        onClick={handleHomeClick}
      >
        Home
      </Button>
    </Box>
  );
};

export default TickerDetailsSummaryPage;
