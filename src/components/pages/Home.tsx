import Box from "@mui/material/Box";
import { ensureTickerInfo } from "../../slices/tickerDetailsSlice";
import { useAppDispatch } from "../../store/hooks";
import SearchBar from "../SearchBar";
import TickerDetailsSummary from "../TickerDetailsSummary";

const Home = () => {
  const dispatch = useAppDispatch();

  const handleOnSearchClick = (search: string) => {
    dispatch(ensureTickerInfo(search));
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      style={{ width: "100%" }}
    >
      <SearchBar onSearch={handleOnSearchClick} />
      <TickerDetailsSummary />
    </Box>
  );
};

export default Home;
