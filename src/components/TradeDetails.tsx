import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useEffect } from "react";
import { fetchTradeDetails } from "../api";
import {
  clearDetails,
  selectDetails,
  selectError,
  selectExchange,
  selectIsFetchInProgress,
  selectTicker,
  setError,
  setTradeDetails,
} from "../slices/tradeDetailsSlice";
import { useAppDispatch, useAppSelector } from "../store/hooks";

const TradeDetails = () => {
  let content;
  const dispatch = useAppDispatch();
  const details = useAppSelector(selectDetails);
  const exchange = useAppSelector(selectExchange);
  const ticker = useAppSelector(selectTicker);
  const isLoading = useAppSelector(selectIsFetchInProgress);
  const error = useAppSelector(selectError);

  const tradeQuantity =
    ticker && ticker.indexOf("/") !== -1 ? ticker.split("/")[0] : "";
  const tradePrice =
    ticker && ticker.indexOf("/") !== -1 ? ticker.split("/")[1] : "";

  useEffect(() => {
    if (ticker && exchange) {
      fetchTradeDetails(ticker, exchange)
        .then((data) => {
          dispatch(setTradeDetails(data));
        })
        .catch(() => {
          dispatch(setError("Something went wrong! Please try again!"));
        });
    }
    return () => {
      dispatch(clearDetails());
    };
  }, [ticker, exchange]);

  if (!ticker || isLoading) {
    content = <CircularProgress size={50} sx={{ marginY: 2 }} />;
  } else {
    content = (
      <TableContainer style={{ margin: "16px 0" }} component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Quantity</TableCell>
              <TableCell align="center">Price</TableCell>
              <TableCell align="center">Type</TableCell>
              <TableCell align="center">Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {details.map((row) => (
              <TableRow key={`${row.price}-${row.quantity}-${row.isBuy}`}>
                <TableCell component="th" scope="row">
                  {row.quantity} {tradeQuantity}
                </TableCell>
                <TableCell align="center">
                  {row.price} {tradePrice}
                </TableCell>
                <TableCell
                  align="center"
                  style={{ color: row.isBuy ? "green" : "red" }}
                >
                  {row.isBuy ? "Buy" : "Sell"}
                </TableCell>
                <TableCell align="center">
                  {new Date(row.time).toLocaleString()}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      {content}
      {error && <p>Some error: {error}</p>}
    </Box>
  );
};

export default TradeDetails;
