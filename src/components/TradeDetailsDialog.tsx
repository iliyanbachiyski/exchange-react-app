import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { capitalizeFirstLetter } from "../helpers";
import { selectExchange, selectTicker } from "../slices/tradeDetailsSlice";
import { useAppSelector } from "../store/hooks";
import TradeDetails from "./TradeDetails";

type OwnProps = {
  open: boolean;
  onClose: () => void;
};

const TradeDetailsDialog = ({ open, onClose }: OwnProps) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
  const exchange = useAppSelector(selectExchange);
  const ticker = useAppSelector(selectTicker);

  return (
    <Dialog
      fullScreen={fullScreen}
      fullWidth
      maxWidth="sm"
      open={open}
      onClose={onClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">
        Trade Details for {ticker} (
        {exchange ? capitalizeFirstLetter(exchange) : ""})
      </DialogTitle>
      <DialogContent>
        <TradeDetails />
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default TradeDetailsDialog;
