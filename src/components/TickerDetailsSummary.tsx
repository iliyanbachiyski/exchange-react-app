import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import { useMemo, useState } from "react";
import TickerInfo, { ExchangeProvider } from "../entities/TickerInfo";
import { capitalizeFirstLetter } from "../helpers";
import {
  selectError,
  selectIsFetchInProgress,
  selectSummary,
} from "../slices/tickerDetailsSlice";
import { setTradeDetailsExchange } from "../slices/tradeDetailsSlice";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import TradeDetailsDialog from "./TradeDetailsDialog";

type Order = "asc" | "desc";

const descendingComparator = (a: TickerInfo, b: TickerInfo) => {
  if (b.price < a.price) {
    return -1;
  }
  if (b.price > a.price) {
    return 1;
  }
  return 0;
};

const getComparator = (
  order: Order
): ((a: TickerInfo, b: TickerInfo) => number) => {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b)
    : (a, b) => -descendingComparator(a, b);
};

type HeadCell = {
  id: keyof TickerInfo;
  label: string;
};

const headCells: HeadCell[] = [
  {
    id: "symbol",
    label: "Ticker",
  },
  {
    id: "provider",
    label: "Exchange",
  },
  {
    id: "price",
    label: "Price",
  },
];

type EnhancedTableProps = {
  onRequestSort: () => void;
  order: Order;
};

const EnhancedTableHead = ({ order, onRequestSort }: EnhancedTableProps) => {
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            sortDirection={headCell.id === "price" ? order : false}
            align={headCell.id === "symbol" ? "left" : "center"}
          >
            {headCell.id === "price" ? (
              <TableSortLabel active direction={order} onClick={onRequestSort}>
                {headCell.label}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const SummaryTable = () => {
  const [tradeDetailsOpen, setTradeDetailsOpen] = useState(false);
  const [order, setOrder] = useState<Order>("asc");
  const dispatch = useAppDispatch();
  const summary = useAppSelector(selectSummary);
  const error = useAppSelector(selectError);
  const isLoading = useAppSelector(selectIsFetchInProgress);

  const priceSymbols = useMemo(() => {
    if (summary) {
      const symbol = Object.values(summary)[0].symbol;
      if (symbol.indexOf("/") !== -1) {
        return {
          baseCurrency: symbol.split("/")[0],
          quoteCurrency: symbol.split("/")[1],
        };
      }
    }
    return null;
  }, [summary]);

  if (error) {
    return (
      <Typography variant="subtitle1" color="red" sx={{ marginY: 2 }}>
        {error}
      </Typography>
    );
  }

  if (!summary) {
    if (isLoading) {
      return <CircularProgress sx={{ marginY: 2 }} size={50} />;
    }
    return null;
  }

  const handlePriceClick = (exchange: ExchangeProvider) => {
    dispatch(setTradeDetailsExchange(exchange));
    setTradeDetailsOpen(true);
  };

  const handleTradeDetailsDialogClose = () => {
    setTradeDetailsOpen(false);
  };

  const handleRequestSort = () => {
    const isAsc = order === "asc";
    setOrder(isAsc ? "desc" : "asc");
  };

  return (
    <>
      {isLoading ? (
        <CircularProgress sx={{ marginY: 2 }} size={50} />
      ) : (
        <TableContainer style={{ margin: "16px 0" }} component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <EnhancedTableHead
              order={order}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {Object.values(summary)
                .slice()
                .sort(getComparator(order))
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={labelId}>
                      <TableCell
                        component="th"
                        scope="row"
                        style={row.isSupported ? {} : { color: "red" }}
                      >
                        {row.isSupported && priceSymbols
                          ? `1 ${priceSymbols.baseCurrency}`
                          : row.symbol.toUpperCase()}
                        <br />
                        {!row.isSupported &&
                          "(The pair is not supported on selected exchange!)"}
                      </TableCell>
                      <TableCell align="center">
                        {capitalizeFirstLetter(row.provider)}
                      </TableCell>
                      <TableCell align="center">
                        <Button
                          variant="text"
                          onClick={() => {
                            handlePriceClick(row.provider);
                          }}
                          disabled={!row.price}
                        >
                          {row.price
                            ? `${Number(row.price).toFixed(2)} ${
                                priceSymbols?.quoteCurrency || ""
                              }`
                            : 0}
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      )}
      <TradeDetailsDialog
        open={tradeDetailsOpen}
        onClose={handleTradeDetailsDialogClose}
      />
    </>
  );
};

export default SummaryTable;
