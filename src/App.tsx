import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { Navigate, Route, Routes } from "react-router-dom";
import TickerDetails from "./components/pages/TickerDetails";
import Home from "./components/pages/Home";
import TickerDetailsSummaryPage from "./components/pages/TickerDetailsSummaryPage";

const App = () => {
  return (
    <Container maxWidth="md">
      <Typography variant="h3" align="center" color="primary" marginY={3}>
        Welcome to Crypto Exchange App
      </Typography>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path=":ticker">
          <Route path="" element={<TickerDetailsSummaryPage />} />
          <Route path="details" element={<TickerDetails />} />
        </Route>
        <Route path="*" element={<Navigate to="/" replace />} />
      </Routes>
    </Container>
  );
};

export default App;
